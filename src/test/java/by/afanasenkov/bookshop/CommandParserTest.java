package by.afanasenkov.bookshop;

import by.afanasenkov.bookshop.config.Config;
import by.afanasenkov.bookshop.tech.command.Command;
import by.afanasenkov.bookshop.tech.command.CommandParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class)
public class CommandParserTest {

    @Autowired
    private CommandParser parser;

    @Test
    public void parseTest()  {
        Command command = parser.parse("class.method?arg1=1&arg2=2");

        assertEquals("class.method", command.getName());
        assertEquals(2, command.getArgs().size());
        assertEquals("1", command.getArgs().get("arg1"));
        assertEquals("2", command.getArgs().get("arg2"));
    }
}
