package by.afanasenkov.bookshop.user.service.impl;

import by.afanasenkov.bookshop.tech.exception.ExecutionException;
import by.afanasenkov.bookshop.tech.session.Session;
import by.afanasenkov.bookshop.tech.session.SessionContext;
import by.afanasenkov.bookshop.user.model.User;
import by.afanasenkov.bookshop.user.repository.UserRepository;
import by.afanasenkov.bookshop.user.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(String name, String password) {
        Session session = SessionContext.getSession();
        if (session.isAuthorized()) {
            session.clear();
        }

        User user = userRepository.findByName(name);
        if (user == null) {
            throw new ExecutionException("Unknown user: " + name);
        }

        if (!StringUtils.equals(DigestUtils.md5Hex(password), user.getPassword())) {
            throw new ExecutionException("Incorrect credentials: " + name);
        }

        user.setPassword("");
        session.setUser(user);

        return user;
    }

    @Override
    public User logout() {
        User user = null;

        Session session = SessionContext.getSession();
        if (session.isAuthorized()) {
            user = session.getUser();
            session.clear();
        }

        return user;
    }

    @Override
    public void exit() {
        System.exit(0);
    }
}
