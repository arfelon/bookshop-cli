package by.afanasenkov.bookshop.user.service;

import by.afanasenkov.bookshop.user.model.User;

public interface UserService {

    User login(String name, String password);

    User logout();

    void exit();
}
