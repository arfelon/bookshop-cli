package by.afanasenkov.bookshop.user.repository.impl;

import by.afanasenkov.bookshop.tech.repository.AbstractRepository;
import by.afanasenkov.bookshop.tech.repository.annotation.InitializableRepository;
import by.afanasenkov.bookshop.user.model.User;
import by.afanasenkov.bookshop.user.repository.UserRepository;
import io.jsondb.JsonDBTemplate;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@InitializableRepository
public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(JsonDBTemplate dbTemplate) {
        super(User.class, dbTemplate);
    }

    @Override
    public User findByName(String name) {
        String query = String.format("/.[name='%s']", name);

        List<User> users = getDbTemplate().find(query, getEntityClass());
        if (CollectionUtils.isEmpty(users)) {
            return null;
        }

        return users.get(0);
    }
}
