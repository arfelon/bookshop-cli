package by.afanasenkov.bookshop.user.repository;

import by.afanasenkov.bookshop.user.model.User;

public interface UserRepository {

    User findByName(String name);
}
