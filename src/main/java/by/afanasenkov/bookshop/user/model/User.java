package by.afanasenkov.bookshop.user.model;

import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;
import lombok.Data;

@Data
@Document(collection = "users", schemaVersion= "1.0")
public class User {

    @Id
    private Long id;

    private String name;

    private String password;

    private boolean admin;
}
