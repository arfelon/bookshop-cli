package by.afanasenkov.bookshop.user.controller;

import by.afanasenkov.bookshop.common.model.Response;
import by.afanasenkov.bookshop.tech.command.annotation.CommandController;
import by.afanasenkov.bookshop.tech.command.annotation.CommandMapping;
import by.afanasenkov.bookshop.user.model.LoginRequest;
import by.afanasenkov.bookshop.user.model.User;
import by.afanasenkov.bookshop.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@CommandController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @CommandMapping
    public Response<User> login(LoginRequest request) {
        return Response.of(userService.login(request.getName(), request.getPassword()));
    }

    @CommandMapping
    public Response<User> logout() {
        return Response.of(userService.logout());
    }

    @CommandMapping
    public void exit() {
        userService.exit();
    }
}
