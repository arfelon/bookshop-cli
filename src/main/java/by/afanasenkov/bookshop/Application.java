package by.afanasenkov.bookshop;

import by.afanasenkov.bookshop.config.Config;
import by.afanasenkov.bookshop.tech.command.CommandDispatcher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);
        CommandDispatcher dispatcher = applicationContext.getBean("commandDispatcher", CommandDispatcher.class);

        requestCommand();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            processCommand(scanner.nextLine(), dispatcher);
            requestCommand();
        }
    }

    private static void requestCommand() {
        System.out.println("Enter your command:");
    }

    private static void processCommand(String command, CommandDispatcher dispatcher) {
        dispatcher.dispatch(command);
    }
}
