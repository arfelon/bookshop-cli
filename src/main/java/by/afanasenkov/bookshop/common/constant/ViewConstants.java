package by.afanasenkov.bookshop.common.constant;

public final class ViewConstants {

    public static final String COMMAND_DONE_VIEW = "done";

    public static final String COMMAND_EXCEPTION_VIEW = "exception";

    private ViewConstants() {
    }
}
