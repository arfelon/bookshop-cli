package by.afanasenkov.bookshop.common.model;

import lombok.Data;

@Data(staticConstructor = "of")
public class Response<T> {

    private final T body;
}
