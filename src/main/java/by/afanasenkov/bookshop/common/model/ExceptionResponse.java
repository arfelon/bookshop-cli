package by.afanasenkov.bookshop.common.model;

import lombok.Data;

@Data
public class ExceptionResponse {

    private String clazz;

    private String message;

    private String trace;
}
