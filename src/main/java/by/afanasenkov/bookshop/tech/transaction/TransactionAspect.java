package by.afanasenkov.bookshop.tech.transaction;

import by.afanasenkov.bookshop.config.ConfigProperties;
import by.afanasenkov.bookshop.tech.log.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Aspect
@Component
public class TransactionAspect {

    private final Supplier<Transaction> transactionSupplier;

    private final Logger logger;

    private final ConfigProperties configProperties;

    @Autowired
    public TransactionAspect(Supplier<Transaction> transactionSupplier, Logger logger, ConfigProperties configProperties) {
        this.transactionSupplier = transactionSupplier;
        this.logger = logger;
        this.configProperties = configProperties;
    }

    @Around("by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.repositoryMethods()")
    public Object transactionSupport(ProceedingJoinPoint joinPoint) throws Throwable {
        Transaction transaction = transactionSupplier.get();

        transaction.start();
        if (configProperties.isDebug()) {
            logger.log("Transaction started: ", transaction);
        }

        try {
            return joinPoint.proceed();
        } finally {
            transaction.finish();
            if (configProperties.isDebug()) {
                logger.log("Transaction finished: ", transaction);
            }

        }
    }
}
