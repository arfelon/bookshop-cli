package by.afanasenkov.bookshop.tech.transaction;

public class Transaction {

    private boolean started;

    public void start() {
        started = true;
    }

    public void finish() {
        started = false;
    }

    public boolean isStarted() {
        return started;
    }
}
