package by.afanasenkov.bookshop.tech.condition;

import by.afanasenkov.bookshop.tech.print.PrintEngineType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Objects;

public abstract class AbstractOnPrintEngineCondition implements Condition {

    private static final String PROPERTY_KEY = "print.engine";

    private final PrintEngineType required;

    protected AbstractOnPrintEngineCondition(PrintEngineType required) {
        this.required = required;
    }

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String propertyValue = context.getEnvironment().getProperty(PROPERTY_KEY);
        if (StringUtils.isEmpty(propertyValue)) {
            return false;
        }

        return Objects.equals(required, PrintEngineType.ofNameOrNull(propertyValue));
    }
}
