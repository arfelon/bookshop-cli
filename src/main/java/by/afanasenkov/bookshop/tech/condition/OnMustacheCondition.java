package by.afanasenkov.bookshop.tech.condition;

import by.afanasenkov.bookshop.tech.print.PrintEngineType;

public class OnMustacheCondition extends AbstractOnPrintEngineCondition {

    public OnMustacheCondition() {
        super(PrintEngineType.MUSTACHE);
    }
}
