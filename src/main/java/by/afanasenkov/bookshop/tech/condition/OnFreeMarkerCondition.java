package by.afanasenkov.bookshop.tech.condition;

import by.afanasenkov.bookshop.tech.print.PrintEngineType;

public class OnFreeMarkerCondition extends AbstractOnPrintEngineCondition {

    public OnFreeMarkerCondition() {
        super(PrintEngineType.FREEMARKER);
    }
}
