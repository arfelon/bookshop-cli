package by.afanasenkov.bookshop.tech.exception;

import by.afanasenkov.bookshop.tech.command.CommandResponsePrinter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExceptionAspect {

    private final CommandResponsePrinter printer;

    @Autowired
    public ExceptionAspect(CommandResponsePrinter printer) {
        this.printer = printer;
    }

    @Around(value = "by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.dispatchMethod()")
    public void aroundDispatchMethod(ProceedingJoinPoint joinPoint) {
        try {
            joinPoint.proceed();
        } catch (Throwable e) {
            printer.printException(e);
        }
    }
}
