package by.afanasenkov.bookshop.tech.pointcuts;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public final class Pointcuts {

    private Pointcuts() {
    }

    @Pointcut("execution(void by.afanasenkov.bookshop.tech.command.CommandDispatcher.dispatch(String))")
    public void dispatchMethod() {
    }

    @Pointcut("execution(@by.afanasenkov.bookshop.tech.security.annotation.UserPermissions * *(..))")
    public void userMethods() {
    }

    @Pointcut("execution(@by.afanasenkov.bookshop.tech.security.annotation.AdminPermissions * *(..))")
    public void adminMethods() {
    }

    @Pointcut("within(by.afanasenkov.bookshop..controller.*)")
    public void controllerMethods() {
    }

    @Pointcut("execution(* by.afanasenkov.bookshop..repository.impl.*.*(..))")
    public void repositoryMethods() {
    }
}

