package by.afanasenkov.bookshop.tech.print;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum PrintEngineType {

    MUSTACHE("mustache", ".mustache"),
    FREEMARKER("freemarker", ".ftl");

    private final String name;
    private final String extension;

    PrintEngineType(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }

    public String getLocation() {
        return "templates/" + name + "/";
    }

    public static PrintEngineType ofNameOrNull(String name) {
        return Arrays.stream(values())
                .filter(r -> StringUtils.equalsIgnoreCase(r.name, name))
                .findFirst()
                .orElse(null);
    }
}
