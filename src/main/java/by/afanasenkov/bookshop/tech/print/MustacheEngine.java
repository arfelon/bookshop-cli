package by.afanasenkov.bookshop.tech.print;

import com.samskivert.mustache.Mustache;

import java.io.Writer;

public class MustacheEngine extends AbstractPrintEngine {

    private final Mustache.Compiler compiler;

    private final Mustache.TemplateLoader loader;

    public MustacheEngine(Mustache.Compiler compiler, Mustache.TemplateLoader loader) {
        super(PrintEngineType.MUSTACHE);
        this.compiler = compiler;
        this.loader = loader;
    }

    @Override
    protected void process(String view, Object object, Writer writer) throws Exception {
        compiler.compile(loader.getTemplate(view)).execute(object, writer);
    }
}
