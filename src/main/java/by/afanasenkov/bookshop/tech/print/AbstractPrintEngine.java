package by.afanasenkov.bookshop.tech.print;

import java.io.PrintWriter;
import java.io.Writer;

public abstract class AbstractPrintEngine implements PrintEngine {

    private final PrintEngineType type;

    protected AbstractPrintEngine(PrintEngineType type) {
        this.type = type;
    }

    @Override
    public void print(String view, Object object) throws Exception {
        Writer writer = new PrintWriter(System.out);
        process(view + type.getExtension(), object, writer);
        writer.flush();
    }

    protected abstract void process(String view, Object object, Writer writer) throws Exception;
}
