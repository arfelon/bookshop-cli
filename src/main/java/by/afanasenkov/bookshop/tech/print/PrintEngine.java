package by.afanasenkov.bookshop.tech.print;

public interface PrintEngine {

    void print(String view, Object object) throws Exception;
}
