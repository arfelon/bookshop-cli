package by.afanasenkov.bookshop.tech.print;

import freemarker.template.Configuration;

import java.io.Writer;

public class FreeMarkerEngine extends AbstractPrintEngine {

    private final Configuration configuration;

    public FreeMarkerEngine(Configuration configuration) {
        super(PrintEngineType.FREEMARKER);
        this.configuration = configuration;
    }

    @Override
    protected void process(String view, Object object, Writer writer) throws Exception {
        configuration.getTemplate(view).process(object, writer);
    }
}
