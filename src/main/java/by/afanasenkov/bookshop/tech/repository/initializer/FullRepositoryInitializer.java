package by.afanasenkov.bookshop.tech.repository.initializer;

import by.afanasenkov.bookshop.config.ConfigProperties;
import by.afanasenkov.bookshop.tech.repository.Repository;
import by.afanasenkov.bookshop.tech.repository.annotation.InitializableRepository;
import io.jsondb.JsonDBTemplate;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Component
@Profile("init")
public class FullRepositoryInitializer extends AbstractRepositoryInitializer {

    private static final String DB_EXTENSION = ".json";

    private static final String DB_INIT_DIRECTORY = "init";

    private final ConfigProperties configProperties;

    @Autowired
    public FullRepositoryInitializer(JsonDBTemplate dbTemplate, ConfigProperties configProperties, @InitializableRepository List<Repository<?>> repositories) {
        super(dbTemplate, repositories);
        this.configProperties = configProperties;
    }

    @Override
    @SneakyThrows
    protected void initRepository(Class<?> entityClass) {
        String dbName = getDbTemplate().getCollectionName(entityClass);
        String dbFile = dbName + DB_EXTENSION;

        Path fromPath = Paths.get(configProperties.getDbPath(), DB_INIT_DIRECTORY, dbFile);
        Path toPath = Paths.get(configProperties.getDbPath(), dbFile);

        Files.copy(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING);

        getDbTemplate().reloadCollection(dbName);
    }
}
