package by.afanasenkov.bookshop.tech.repository;

public interface Repository<T> {

    Class<T> getEntityClass();
}
