package by.afanasenkov.bookshop.tech.repository;

import io.jsondb.JsonDBTemplate;

public abstract class AbstractRepository<T> implements Repository<T> {

    private final Class<T> entityClass;

    private final JsonDBTemplate dbTemplate;

    protected AbstractRepository(Class<T> entityClass, JsonDBTemplate dbTemplate) {
        this.entityClass = entityClass;
        this.dbTemplate = dbTemplate;
    }

    @Override
    public Class<T> getEntityClass() {
        return entityClass;
    }

    protected JsonDBTemplate getDbTemplate() {
        return dbTemplate;
    }
}
