package by.afanasenkov.bookshop.tech.repository.initializer;

import by.afanasenkov.bookshop.tech.repository.Repository;
import by.afanasenkov.bookshop.tech.repository.annotation.InitializableRepository;
import io.jsondb.JsonDBTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile("default")
public class DefaultRepositoryInitializer extends AbstractRepositoryInitializer {

    @Autowired
    public DefaultRepositoryInitializer(JsonDBTemplate dbTemplate, @InitializableRepository List<Repository<?>> repositories) {
        super(dbTemplate, repositories);
    }

    @Override
    protected void initRepository(Class<?> entityClass) {
        if (!getDbTemplate().collectionExists(entityClass)) {
            getDbTemplate().createCollection(entityClass);
        }
    }
}
