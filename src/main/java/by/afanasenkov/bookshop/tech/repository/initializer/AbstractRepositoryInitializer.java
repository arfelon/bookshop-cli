package by.afanasenkov.bookshop.tech.repository.initializer;

import by.afanasenkov.bookshop.tech.repository.Repository;
import io.jsondb.JsonDBTemplate;
import org.apache.commons.collections4.ListUtils;

import javax.annotation.PostConstruct;
import java.util.List;

public abstract class AbstractRepositoryInitializer {

    private final JsonDBTemplate dbTemplate;

    private final List<Repository<?>> repositories;

    protected AbstractRepositoryInitializer(JsonDBTemplate dbTemplate, List<Repository<?>> repositories) {
        this.dbTemplate = dbTemplate;
        this.repositories = ListUtils.emptyIfNull(repositories);
    }

    @PostConstruct
    public void init() {
        for (Repository<?> repository : repositories) {
            initRepository(repository.getEntityClass());
        }
    }

    protected abstract void initRepository(Class<?> entityClass);

    protected JsonDBTemplate getDbTemplate() {
        return dbTemplate;
    }
}
