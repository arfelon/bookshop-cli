package by.afanasenkov.bookshop.tech.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

@Data
@AllArgsConstructor
public class CommandDefinition<T> {

    private Object controller;

    private Method method;

    private Class<T> requestClass;

    private String responseView;
}
