package by.afanasenkov.bookshop.tech.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

@Component
public class CommandDispatcher {

    private final CommandRegistry registry;

    private final CommandParser parser;

    private final CommandRequestMarshaller marshaller;

    private final CommandResponsePrinter printer;

    @Autowired
    public CommandDispatcher(CommandRegistry registry, CommandParser parser, CommandRequestMarshaller marshaller, CommandResponsePrinter printer) {
        this.registry = registry;
        this.parser = parser;
        this.marshaller = marshaller;
        this.printer = printer;
    }

    private Command parse(String uri) {
        return parser.parse(uri);
    }

    private CommandDefinition<?> findDefinition(Command command) {
        return registry.findDefinition(command);
    }

    private boolean isVoidRequest(CommandDefinition<?> definition) {
        return definition.getRequestClass() == Void.class;
    }

    private Object createRequest(Command command, CommandDefinition<?> definition) {
        return marshaller.marshal(command, definition);
    }

    private Object execute(Command command, CommandDefinition<?> definition) {
        Object response;
        if (isVoidRequest(definition)) {
            response = ReflectionUtils.invokeMethod(definition.getMethod(), definition.getController());
        } else {
            response = ReflectionUtils.invokeMethod(definition.getMethod(), definition.getController(),
                    createRequest(command, definition));
        }

        return response;
    }

    private void print(Command command, CommandDefinition<?> definition, Object response) {
        printer.print(command, definition, response);
    }

    public void dispatch(String uri) {
        Command command = parse(uri);
        CommandDefinition<?> definition = findDefinition(command);

        Object response = execute(command, definition);

        print(command, definition, response);
    }
}
