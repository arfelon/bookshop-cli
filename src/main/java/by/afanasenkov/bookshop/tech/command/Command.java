package by.afanasenkov.bookshop.tech.command;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class Command {

    private final String name;

    @Singular
    private final Map<String, String> args;
}
