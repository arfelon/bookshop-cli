package by.afanasenkov.bookshop.tech.command;

import by.afanasenkov.bookshop.tech.command.annotation.CommandController;
import by.afanasenkov.bookshop.tech.command.annotation.CommandMapping;
import by.afanasenkov.bookshop.tech.command.exception.CommandMappingException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CommandRegistry implements ApplicationContextAware {

    private static final String CONTROLLER_POSTFIX = "Controller";

    private static final String COMMAND_NAME_SEPARATOR = ".";

    private ApplicationContext applicationContext;

    private Map<String, CommandDefinition<?>> definitions = new HashMap<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    public void init() {
        Collection<Object> controllers = applicationContext.getBeansWithAnnotation(CommandController.class).values();
        for (Object controller : controllers) {
            initFromController(controller);
        }
    }

    private Class<?> getControllerClass(Object controller) {
        Class<?> controllerClass;
        if (AopUtils.isAopProxy(controller)) {
            controllerClass = AopUtils.getTargetClass(controller);
        } else {
            controllerClass = controller.getClass();
        }

        return controllerClass;
    }

    private String getControllerName(Class<?> controllerClass) {
        String controllerName = StringUtils.uncapitalize(controllerClass.getSimpleName());
        controllerName = StringUtils.removeEndIgnoreCase(controllerName, CONTROLLER_POSTFIX);

        if (StringUtils.isBlank(controllerName)) {
            throw new CommandMappingException("Invalid controller name: " + controllerClass.getName());
        }

        return controllerName;
    }

    private List<Method> getMappedMethods(Class<?> controllerClass) {
        Method[] methods = controllerClass.getDeclaredMethods();
        if (ArrayUtils.isEmpty(methods)) {
            return Collections.emptyList();
        }

        return Stream.of(methods)
                .filter(m -> m.isAnnotationPresent(CommandMapping.class))
                .collect(Collectors.toList());
    }

    private CommandMapping getMappingAnnotation(Method method) {
        return method.getAnnotation(CommandMapping.class);
    }

    private String getMethodName(Method method) {
        return StringUtils.uncapitalize(method.getName());
    }

    private String getCommandName(String controllerName, String methodName) {
        return controllerName + COMMAND_NAME_SEPARATOR + methodName;
    }

    private Class<?> getRequestClass(Method method) {
        Class<?> requestClass;
        if (method.getParameterCount() == 0) {
            requestClass = Void.class;
        } else if (method.getParameterCount() == 1) {
            requestClass = method.getParameterTypes()[0];
        } else {
            throw new CommandMappingException("Request defined incorrectly: " + method.getDeclaringClass().getName());
        }

        return requestClass;
    }

    private String getResponseView(Method method, String defaultView) {
        return StringUtils.defaultIfEmpty(StringUtils.trimToNull(getMappingAnnotation(method).view()), defaultView);
    }

    private <T> CommandDefinition<T> createDefinition(Object controller, Method method, Class<T> requestClass, String responseView) {
        return new CommandDefinition<>(controller, method, requestClass, responseView);
    }

    private void addCommand(String name, CommandDefinition<?> definition) {
        if (definitions.containsKey(name)) {
            throw new CommandMappingException("Command already defined: " + name);
        }

        definitions.put(name, definition);
    }

    private void initFromController(Object controller) {
        Class<?> controllerClass = getControllerClass(controller);
        String controllerName = getControllerName(controllerClass);

        List<Method> methods = getMappedMethods(controllerClass);
        for (Method method : methods) {
            String methodName = getMethodName(method);
            String commandName = getCommandName(controllerName, methodName);

            Class<?> requestClass = getRequestClass(method);
            String responseView = getResponseView(method, commandName);

            addCommand(commandName, createDefinition(controller, method, requestClass, responseView));
        }
    }

    public CommandDefinition<?> findDefinition(Command command) {
        if (StringUtils.isBlank(command.getName())) {
            throw new CommandMappingException("Command name not specified: " + command.getName());
        }

        CommandDefinition<?> definition = definitions.get(command.getName());
        if (definition == null) {
            throw new CommandMappingException("Unknown command: " + command.getName());
        }

        return definition;
    }
}
