package by.afanasenkov.bookshop.tech.command.exception;

public class CommandMappingException extends RuntimeException {

    public CommandMappingException() {
    }

    public CommandMappingException(String message) {
        super(message);
    }

    public CommandMappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandMappingException(Throwable cause) {
        super(cause);
    }
}
