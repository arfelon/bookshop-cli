package by.afanasenkov.bookshop.tech.command;

import by.afanasenkov.bookshop.tech.command.exception.CommandParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CommandRequestMarshaller {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public <T> T marshal(Command command, CommandDefinition<T> definition) {
        try {
            return MAPPER.readerFor(definition.getRequestClass()).readValue(MAPPER.writeValueAsString(command.getArgs()));
        } catch (IOException e) {
            throw new CommandParseException("Error creating command request: " + command.getName(), e);
        }
    }
}
