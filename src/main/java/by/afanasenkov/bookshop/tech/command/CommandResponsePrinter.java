package by.afanasenkov.bookshop.tech.command;

import by.afanasenkov.bookshop.common.constant.ViewConstants;
import by.afanasenkov.bookshop.common.model.ExceptionResponse;
import by.afanasenkov.bookshop.tech.command.exception.CommandPrintException;
import by.afanasenkov.bookshop.tech.print.PrintEngine;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandResponsePrinter {

    private final PrintEngine printEngine;

    @Autowired
    public CommandResponsePrinter(PrintEngine printEngine) {
        this.printEngine = printEngine;
    }

    public void print(Command command, CommandDefinition<?> definition, Object response) {
        try {
            print(definition.getResponseView(), response);
        } catch (Exception e) {
            throw new CommandPrintException("Error printing command response: " + command.getName(), e);
        }
    }

    public void printException(Throwable problem) {
        ExceptionResponse response = new ExceptionResponse();
        response.setClazz(problem.getClass().getName());
        response.setMessage(problem.getMessage());
        response.setTrace(ExceptionUtils.getStackTrace(problem));

        try {
            print(ViewConstants.COMMAND_EXCEPTION_VIEW, response);
        } catch (Exception e) {
            throw new CommandPrintException("Error printing exception");
        }
    }

    private void print(String view, Object object) throws Exception {
        printEngine.print(view, object);
    }
}
