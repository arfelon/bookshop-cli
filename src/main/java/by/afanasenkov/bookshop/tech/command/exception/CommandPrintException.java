package by.afanasenkov.bookshop.tech.command.exception;

public class CommandPrintException extends RuntimeException {

    public CommandPrintException() {
    }

    public CommandPrintException(String message) {
        super(message);
    }

    public CommandPrintException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandPrintException(Throwable cause) {
        super(cause);
    }
}
