package by.afanasenkov.bookshop.tech.command;

import by.afanasenkov.bookshop.tech.command.exception.CommandParseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CommandParser {

    private static final Pattern ARGS_PATTERN = Pattern.compile("([A-z][A-z\\d_]*)=([^&]*)");

    private static final Pattern STRUCTURE_PATTERN = Pattern.compile("([A-z][A-z\\d_]*\\.[A-z][A-z\\d_]*)(\\?.*)?");

    public Command parse(String uri) {
        if (StringUtils.isEmpty(uri)) {
            throw new CommandParseException("Empty command");
        }

        Matcher structureMatcher = STRUCTURE_PATTERN.matcher(uri);
        if (!structureMatcher.matches()) {
            throw new CommandParseException("Invalid command");
        }

        Command.CommandBuilder builder = Command.builder();
        builder.name(structureMatcher.group(1));

        String args = StringUtils.removeStart(structureMatcher.group(2), "?");
        if (StringUtils.isNotEmpty(args)) {
            Matcher argsMatcher = ARGS_PATTERN.matcher(args);
            while (argsMatcher.find()) {
                builder.arg(argsMatcher.group(1), argsMatcher.group(2));
            }
        }

        return builder.build();
    }
}
