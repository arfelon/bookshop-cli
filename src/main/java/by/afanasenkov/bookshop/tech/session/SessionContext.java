package by.afanasenkov.bookshop.tech.session;

public class SessionContext {

    private static ThreadLocal<Session> CONTEXT = ThreadLocal.withInitial(Session::new);

    public static Session getSession() {
        return CONTEXT.get();
    }
}
