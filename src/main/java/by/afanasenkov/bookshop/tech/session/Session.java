package by.afanasenkov.bookshop.tech.session;

import by.afanasenkov.bookshop.user.model.User;
import lombok.ToString;

@ToString
public class Session {

    private User user;

    public boolean isAuthorized() {
        return user != null;
    }

    public void clear() {
        setUser(null);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
