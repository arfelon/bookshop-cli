package by.afanasenkov.bookshop.tech.security;

import by.afanasenkov.bookshop.tech.exception.ExecutionException;
import by.afanasenkov.bookshop.tech.session.Session;
import by.afanasenkov.bookshop.tech.session.SessionContext;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(SecurityAspect.SECURITY_PRECEDENCE)
public class SecurityAspect {

    public static final int SECURITY_PRECEDENCE = Ordered.HIGHEST_PRECEDENCE + 2;

    @Before("by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.userMethods()")
    public void beforeUserMethods() {
        Session session = SessionContext.getSession();
        if (!session.isAuthorized()) {
            throw new ExecutionException("You need login first");
        }
    }

    @Before("by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.adminMethods()")
    public void beforeAdminMethods() {
        Session session = SessionContext.getSession();
        if (!session.isAuthorized()) {
            throw new ExecutionException("You need login first");
        }
        if (!session.getUser().isAdmin()) {
            throw new ExecutionException("You have not enough permissions to execute command");
        }
    }
}
