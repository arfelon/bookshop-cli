package by.afanasenkov.bookshop.book.service;

import by.afanasenkov.bookshop.book.model.Book;

import java.util.List;

public interface BookService {

    List<Book> list();

    Book get(Long id);

    void add(Book book);

    void update(Book book);

    void delete(Long id);
}
