package by.afanasenkov.bookshop.book.service.impl;

import by.afanasenkov.bookshop.book.model.Book;
import by.afanasenkov.bookshop.book.repository.BookRepository;
import by.afanasenkov.bookshop.book.service.BookService;
import by.afanasenkov.bookshop.tech.exception.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> list() {
        return bookRepository.findAll();
    }

    @Override
    public Book get(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public void add(Book book) {
        if (bookRepository.findById(book.getId()) != null) {
            throw new ExecutionException("Book already registered: " + book.getId());
        }

        bookRepository.save(book);
    }

    @Override
    public void update(Book book) {
        if (bookRepository.findById(book.getId()) == null) {
            throw new ExecutionException("Book not registered: " + book.getId());
        }

        bookRepository.save(book);
    }

    @Override
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }
}
