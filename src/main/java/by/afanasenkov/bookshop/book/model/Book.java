package by.afanasenkov.bookshop.book.model;

import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;
import lombok.Data;

@Data
@Document(collection = "books", schemaVersion= "1.0")
public class Book {

    @Id
    private Long id;

    private String title;

    private String author;
}
