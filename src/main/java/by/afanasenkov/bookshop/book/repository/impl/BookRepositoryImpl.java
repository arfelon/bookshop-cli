package by.afanasenkov.bookshop.book.repository.impl;

import by.afanasenkov.bookshop.book.model.Book;
import by.afanasenkov.bookshop.book.repository.BookRepository;
import by.afanasenkov.bookshop.tech.repository.AbstractRepository;
import by.afanasenkov.bookshop.tech.repository.annotation.InitializableRepository;
import io.jsondb.JsonDBTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@InitializableRepository
public class BookRepositoryImpl extends AbstractRepository<Book> implements BookRepository {

    @Autowired
    public BookRepositoryImpl(JsonDBTemplate dbTemplate) {
        super(Book.class, dbTemplate);
    }

    @Override
    public List<Book> findAll() {
        return getDbTemplate().findAll(getEntityClass());
    }

    @Override
    public Book findById(Long id) {
        return getDbTemplate().findById(id, getEntityClass());
    }

    @Override
    public Book save(Book book) {
        getDbTemplate().upsert(book);
        return book;
    }

    @Override
    public void deleteById(Long id) {
        Book book = new Book();
        book.setId(id);

        getDbTemplate().remove(book, getEntityClass());
    }
}
