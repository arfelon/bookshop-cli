package by.afanasenkov.bookshop.book.repository;

import by.afanasenkov.bookshop.book.model.Book;

import java.util.List;

public interface BookRepository {

    List<Book> findAll();

    Book findById(Long id);

    Book save(Book book);

    void deleteById(Long id);
}
