package by.afanasenkov.bookshop.book.controller;

import by.afanasenkov.bookshop.book.model.Book;
import by.afanasenkov.bookshop.book.model.BookRequest;
import by.afanasenkov.bookshop.book.service.BookService;
import by.afanasenkov.bookshop.common.constant.ViewConstants;
import by.afanasenkov.bookshop.common.model.Response;
import by.afanasenkov.bookshop.tech.command.annotation.CommandController;
import by.afanasenkov.bookshop.tech.command.annotation.CommandMapping;
import by.afanasenkov.bookshop.tech.security.annotation.AdminPermissions;
import by.afanasenkov.bookshop.tech.security.annotation.UserPermissions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@CommandController
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @UserPermissions
    @CommandMapping
    public Response<List<Book>> list() {
        return Response.of(bookService.list());
    }

    @UserPermissions
    @CommandMapping
    public Response<Book> get(BookRequest request) {
        return Response.of(bookService.get(request.getId()));
    }

    @AdminPermissions
    @CommandMapping(view = ViewConstants.COMMAND_DONE_VIEW)
    public void add(Book book) {
        bookService.add(book);
    }

    @AdminPermissions
    @CommandMapping(view = ViewConstants.COMMAND_DONE_VIEW)
    public void update(Book book) {
        bookService.update(book);
    }

    @AdminPermissions
    @CommandMapping(view = ViewConstants.COMMAND_DONE_VIEW)
    public void delete(BookRequest request) {
        bookService.delete(request.getId());
    }
}
