package by.afanasenkov.bookshop.config;

import by.afanasenkov.bookshop.tech.transaction.Transaction;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;

import java.util.function.Supplier;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("by.afanasenkov.bookshop")
@PropertySource("classpath:application.properties")
public class Config {

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Transaction transaction() {
        return new Transaction();
    }

    @Bean
    public Supplier<Transaction> transactionSupplier() {
        return this::transaction;
    }
}
