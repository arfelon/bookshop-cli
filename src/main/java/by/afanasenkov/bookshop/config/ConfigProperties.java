package by.afanasenkov.bookshop.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ConfigProperties {

    @Value("${mode.debug}")
    private boolean debug;

    @Value("${db.path}")
    private String dbPath;
}
