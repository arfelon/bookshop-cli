package by.afanasenkov.bookshop.config.print;

import by.afanasenkov.bookshop.tech.condition.OnMustacheCondition;
import by.afanasenkov.bookshop.tech.print.MustacheEngine;
import by.afanasenkov.bookshop.tech.print.PrintEngine;
import by.afanasenkov.bookshop.tech.print.PrintEngineType;
import com.samskivert.mustache.Mustache;
import org.springframework.boot.autoconfigure.mustache.MustacheResourceTemplateLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

@Configuration
@Conditional(OnMustacheCondition.class)
public class MustacheConfig {

    @Bean
    public Mustache.Compiler mustacheCompiler() {
        return Mustache.compiler().defaultValue("");
    }

    @Bean
    public Mustache.TemplateLoader mustacheTemplateLoader(ResourceLoader resourceLoader) {
        MustacheResourceTemplateLoader templateLoader = new MustacheResourceTemplateLoader(PrintEngineType.MUSTACHE.getLocation(), "");
        templateLoader.setResourceLoader(resourceLoader);

        return templateLoader;
    }

    @Bean
    public PrintEngine printEngine(Mustache.Compiler compiler, Mustache.TemplateLoader templateLoader) {
        return new MustacheEngine(compiler, templateLoader);
    }
}
