package by.afanasenkov.bookshop.config.print;

import by.afanasenkov.bookshop.tech.condition.OnFreeMarkerCondition;
import by.afanasenkov.bookshop.tech.print.FreeMarkerEngine;
import by.afanasenkov.bookshop.tech.print.PrintEngine;
import by.afanasenkov.bookshop.tech.print.PrintEngineType;
import freemarker.cache.TemplateLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.ui.freemarker.SpringTemplateLoader;

@Configuration
@Conditional(OnFreeMarkerCondition.class)
public class FreeMarkerConfig {

    @Bean
    public TemplateLoader freeMarkerTemplateLoader(freemarker.template.Configuration configuration, ResourceLoader resourceLoader) {
        TemplateLoader templateLoader = new SpringTemplateLoader(resourceLoader, PrintEngineType.FREEMARKER.getLocation());
        configuration.setTemplateLoader(templateLoader);

        return templateLoader;
    }

    @Bean
    public PrintEngine printEngine(freemarker.template.Configuration configuration) {
        return new FreeMarkerEngine(configuration);
    }
}
