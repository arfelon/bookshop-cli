package by.afanasenkov.bookshop.config.db;

import by.afanasenkov.bookshop.config.ConfigProperties;
import io.jsondb.JsonDBTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbConfig {

    @Bean
    public JsonDBTemplate dbTemplate(ConfigProperties configProperties) {
        return new JsonDBTemplate(configProperties.getDbPath(), "by.afanasenkov.bookshop");
    }
}
