bookshop-cli

Change json.db.path in application.properties
Use profile init for initialize db from json.db.path/init directory or default for usual work

Feel free to set command.print.engine and choose any of supported view print engines (freemarker, mustache)

Available commands:

user.login?name=Andrey&password=123
user.login?name=Dmitry&password=123
user.logout
user.exit

book.list
book.get?id=3
book.add?id=3&title=Best book&author=Karl Gustoff Mergolts
book.update?id=3&title=Book of my life&author=Karl Gustoff Mergolts
book.delete?id=3
